$(function(){
    $.nette.init();
    updateNewsFeed(); 

    $('a').hover(
        function()
        {
            $(this).fadeTo('fast',0.65);
        },
        function()
        {
            $(this).fadeTo('fast',1);
        }
    );
    
    $('.pageNumber').hover(
        function()
        {
            $(this).css('color','#222');
        },
        function()
        {
            $(this).css('color','#CD6600');
        }
    );
    
    setInterval(function()
    {
        $('.flashMessage').fadeTo('fast',0);
    },2500);   
   
});

function updateNewsFeed()
{
    var url = $('#mainScript').attr('src');
    url = url.substr(0,url.lastIndexOf('/'));
    
    $.ajax(
    {
        url: url + '/../RSS.php?url=http://www.novinky.cz/rss2&limit=8',
        context: document.body,
        success: function(response)
        {
            var index = response.indexOf('<body>') - 1;
            var data = response.substr(index).replace('<body>','').replace('</body>','');
            $('#newsFeed').html(data);
            $('aside').first().css('display','block');
        }
    });
}


