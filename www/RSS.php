<!DOCTYPE html>

<!--/**  @author Lukáš Kořínek <korinek111@gmail.com> */-->

<html>
    
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <ul>
    <?php

    if(isset($_GET['url']))
    {
        $xmlDocument = new DOMDocument();
        $xmlDocument->load($_GET['url']);
        $limit = 100;
        if(isset($_GET['limit']))
        {
            $limit = 10;
        }

        $i = 0;
        foreach($xmlDocument->getElementsByTagName('item') as $node)
        {
            $link = NULL;
            $title = NULL;
            
            foreach($node->childNodes as $child)
            {
                if($child->nodeName == 'link') $link = $child->nodeValue;
                else if($child->nodeName == 'title') $title = $child->nodeValue;
            }
           
            if($link != NULL and $title != NULL)
            echo "<li><a href='$link' target='_blank'>$title</a></li>";
            
            if(++$i == $limit )
            {
                break;
            }
        }
    }

    ?>
       </ul>
    </body>
    
</html>
