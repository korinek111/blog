<?php

/**@author Lukáš Kořínek <korinek111@gmail.com>*/

namespace App\Model;

use Nette;

/**
 * Main model class for all operations with database - articles, authors and comments
 */
class ArticleManager 
{
    
    	/** @var Nette\Database\Context */
	private $database;
        
        /**
         * Private method. Changes a plain text into HTML
         * @param string $text Text to be converted
         * @return string
         */
        public static function setParagraphs($text)
        {
            $text = trim($text);
            $text = '<p>' . str_replace('\n', '</p><p>', $text);
            return $text;
        }

        /**
         * Constructs the article manager with database as a parameter
         * @param Nette\Database\Context $database
         */
	public function __construct(Nette\Database\Context $database)
	{
            $this->database = $database;
	}
        
        /**
         * Creates a new article
         * @param string $header Header of the article
         * @param string $imgPath Path to the uploaded image
         * @param uint $author Id of the author
         * @param string $content Content of the article
         */
        public function createArticle($header, $imgPath, $author, $content)
        {
                $this->database->table('ARTICLES')->insert(array
                        (
                            'HEADER' => $header,
                            'IMAGE' => $imgPath,
                            'AUTHOR_ID' => $author,
                            'CONTENT' => $content                            
                        ));
        }

        /**
         * Edits an existing article specified by its articleId
         * @param uint $articleId Id of the article
         * @param string $header New header
         * @param string $imgPath Path to the new image
         * @param string $content New content of the article
         */
        public function editArticle($articleId, $header, $imgPath, $content)
        {
                $this->database->table('ARTICLES')->where('ARTICLE_ID',$articleId)->update(array
                (
                    'HEADER' => $header,
                    'IMAGE' => $imgPath,
                    'CONTENT' => $content                            
                ));
        }

        /**
         * Search function. Finds articles with content, header or author
         * specified by the needle
         * @param string $needle String to be searched for
         * @param uint $offset Offset for the limit function. For paging purposes 
         * @param uint $count Count of articles to be fetched. For paging purposes
         * @return type
         */
        public function findArticle($needle,$offset,$count = 5)
        {
            $q = 'SELECT art.ARTICLE_ID,art.CONTENT,art.HEADER,art.AUTHOR_ID,art.DATE_ADDED,art.IMAGE,aut.NICK '
                . 'FROM ARTICLES art '
                . 'INNER JOIN AUTHORS aut '
                . 'ON art.AUTHOR_ID = aut.AUTHOR_ID '
                . 'WHERE art.HEADER LIKE "%' . $needle . '%" '
                . 'OR art.CONTENT LIKE "%' . $needle . '%" '
                . 'OR aut.NICK LIKE "%' . $needle . '%" '
                . 'ORDER BY ARTICLE_ID DESC '
                . 'LIMIT ' . $offset . ',' . $count;

            return $this->database->query($q)->fetchAll();
        }
        
        /**
         * Method for article fetching.
         * If both id and author are null, returns all articles
         * If author is NOT null, returns articles for the author
         * And if author is null and the id is NOT null, returns an article with the id
         * @param uint $id
         * @param uint $author
         * @param uint $offset
         * @param uint $count
         * @return Object[]/Object Can be both array or a single object      */
        public function getArticles($id,$author,$offset, $count = 5)
        {
            if($id == NULL && $author == NULL)
            {
                $q = 'SELECT art.ARTICLE_ID,art.CONTENT,art.HEADER,art.AUTHOR_ID,art.DATE_ADDED,art.IMAGE,aut.NICK '
                    . 'FROM ARTICLES art '
                    . 'INNER JOIN AUTHORS aut ON art.AUTHOR_ID = aut.AUTHOR_ID '
                    . 'ORDER BY ARTICLE_ID DESC '
                    . 'LIMIT ' . $offset . ',' . $count;

                return $this->database->query($q)->fetchAll();
            }
            else
            {
                if($author != NULL)
                {
                    $q = 'SELECT art.ARTICLE_ID,art.CONTENT,art.HEADER,art.AUTHOR_ID,art.DATE_ADDED,art.IMAGE '
                    . 'FROM ARTICLES art '
                    . 'WHERE AUTHOR_ID = ' . $author . ' '
                    . 'ORDER BY ARTICLE_ID DESC '
                    . 'LIMIT ' . $offset . ',' . $count;
                    return $this->database->query($q)->fetchAll();
                }
                else
                {
                    $q = 'SELECT art.ARTICLE_ID,art.CONTENT,art.HEADER,art.AUTHOR_ID,art.DATE_ADDED,art.IMAGE,aut.NICK '
                    . 'FROM ARTICLES art '
                    . 'INNER JOIN AUTHORS aut ON art.AUTHOR_ID = aut.AUTHOR_ID '
                    . 'WHERE ARTICLE_ID = ' . $id . ' '
                    . 'ORDER BY ARTICLE_ID DESC '
                    . 'LIMIT ' . $offset . ',' . $count;
                    return $this->database->query($q)->fetch();
                }
            }
        }    
        
        /**
         * Gets counts of articles in the database.
         * If author is NOT null, gets only his articles
         * @param uint $author Id of the author
         * @return uint       */
        public function getArticleCount($author)
        {
            if($author != NULL)
            {
                return count($this->database->table('ARTICLES')->where('AUTHOR_ID',$author)->fetchAll());
            }
            else
            {
                return count($this->database->table('ARTICLES')->fetchAll());
            }
        }
        
        public function getArticleCountForSearch($needle)
        {
            $q = 'SELECT COUNT(*) AS C '
                . 'FROM ARTICLES art '
                . 'INNER JOIN AUTHORS aut '
                . 'ON art.AUTHOR_ID = aut.AUTHOR_ID '
                . 'WHERE art.HEADER LIKE "%' . $needle . '%" '
                . 'OR art.CONTENT LIKE "%' . $needle . '%" '
                . 'OR aut.NICK LIKE "%' . $needle . '%" ';
            return $this->database->query($q)->fetch()['C'];
        }
        
        /**
         * Deletes an article with the specified articleId
         * @param uint $articleId Id of the article
         */
        public function deleteArticle($articleId)
        {
            $this->database->query('DELETE FROM COMMENTS WHERE ARTICLE_ID = ?',$articleId);
            $this->database->query('DELETE FROM ARTICLES WHERE ARTICLE_ID = ?',$articleId);
        }
        
        /**
         * Gets all authors or author with the specified authorId, depending on its value (null = all authors)
         * @param uint $authorId Id of the author
         * @return Object[]/Object Can be both array or a single object
         */
        public function getAuthors($authorId = NULL)
        {
            if($authorId == NULL)
            {
                return $this->database->table('AUTHORS')->fetchAll();
            }
            else
            {
                return $this->database->table('AUTHORS')->where('AUTHOR_ID',$authorId)->fetch();
            }
        }
        
        /**
         * Gets comments for all articles, which have their id between the min and max
         * @param uint $minArticleId
         * @param uint $maxArticleId
         * @return Object[]
         */
        public function getComments($minArticleId,$maxArticleId)
        {
            if($minArticleId > $maxArticleId)
            {
                $i = $minArticleId;
                $minArticleId = $maxArticleId;
                $maxArticleId = $i;
            }
            
            $q = 'SELECT * '
                . 'FROM COMMENTS c '
                . 'INNER JOIN AUTHORS a '
                . 'USING(AUTHOR_ID) '
                . 'WHERE ARTICLE_ID BETWEEN ' . $minArticleId . ' '
                . 'AND ' . $maxArticleId;
            
            return $this->database->query($q)->fetchAll();
        }
        
        /**
         * Returns a comment with specified ID
         * @param uint $commentId Id of the comment
         * @return Object
         */
        public function getCommentById($commentId)
        {
            return $this->database->table('COMMENTS')->where('COMMENT_ID',$commentId)->fetch();
        }
        
        /**
         * Adds a new comment into the database
         * @param uint $articleId Id of the comment's article
         * @param string $remoteIP IP address of the client
         * @param uint $authorId Id of the author of the comment
         * @param string $content Content of the comment  */
        public function addComment($articleId,$remoteIP,$authorId,$content)
        {            
                $this->database->table('COMMENTS')->insert(array
            (
            'ARTICLE_ID' => $articleId,
            'IP_ADDRESS' => $remoteIP,
            'AUTHOR_ID' => $authorId,
            'CONTENT' => $content
            ));
        }
        
        /**
         * Edits an existing comment
         * @param type $commentId Id of the comment
         * @param type $content New content
         */
        public function editComment($commentId,$content)
        {
             $this->database->table('COMMENTS')->where('COMMENT_ID',$commentId)->update(array
             (
                'CONTENT' => $content
              ));
        }
        
        /**
         * Deletes the comment specified by commentId
         * @param uint $commentId Id of the comment
         */
        public function deleteComment($commentId)
        {
            $this->database->table('COMMENTS')->where('COMMENT_ID',$commentId)->delete();
        }
        
         /**
        * Gets count of comments existing for each article
        * @param Object[] $articles Array of articles
        * @return int[] Returns an associative array of integers, where the key is the ARTICLE_ID of the article
        */
        public function getCommentCounts($articles)
        {
            $commentCounts = array();

            if(count($articles) > 0)
            {
                foreach($articles as $article)
                {
                    $commentCounts[$article->ARTICLE_ID] = 0;
                }

                $comments = $this->getComments($articles[count($articles) - 1]->ARTICLE_ID,$articles[0]->ARTICLE_ID);
                foreach ($comments as $comment) 
                {
                    $commentCounts[$comment->ARTICLE_ID]++;
                }
            }

            return $commentCounts;
        }
    
}
