<?php

/**@author Lukáš Kořínek <korinek111@gmail.com>*/

namespace App\Model;

use Nette;
use Nette\Utils\Strings;
use Nette\Security\Passwords;
use Nette\Mail\Message;


/**
 * Manages users
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{
	/** @var Nette\Database\Context */
	private $database;


        /**
         * Contructs the object with a database as a parameter
         * @param Nette\Database\Context $database
         */
	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}
        
        /**
         * Creates a new user
         * @param string $email Email of the user
         * @param string $nick Nick of the user
         * @param string $password User's unhashed password
         * @return string
         */
        public function addUser($email,$nick,$password)
        {
            if($this->database->table('AUTHORS')->where('NICK',$nick)->fetch() != NULL)
            {
                return 'Uživatel s touto přezdívkou již existuje!';
            }
            else if($this->database->table('AUTHORS')->where('EMAIL',$email)->fetch() != NULL)
            {
                return 'Uživatel s touto emailovou adresou již existuje!';
            }
            else
            {
                $this->database->table('AUTHORS')->insert(array('AUTHOR_ID' => NULL,'NICK' => $nick,'PASSWORD' => Nette\Security\Passwords::hash($password),'EMAIL' => $email));
                return 'OK';
            }            
        }

	
        /**
         * Authenticates the user
         * @param array $credentials Email and password
         * @return \Nette\Security\Identity Identity of the user
         * @throws Nette\Security\AuthenticationException
         */
        public function authenticate(array $credentials)
        {
            list($email, $password) = $credentials;
            $user = $this->database->table('AUTHORS')->where('EMAIL',$email)->fetch();

            if ($user == NULL || !Nette\Security\Passwords::verify($password, $user->PASSWORD)) 
            {
                throw new Nette\Security\AuthenticationException('Špatný email nebo heslo');
            }
            
            $data = array();
            array_push($data, $user->NICK);
            array_push($data, $user->EMAIL);
            
            return new Nette\Security\Identity($user->AUTHOR_ID, 'AUTHOR',$data);
        }
    
        /**
         * Creates a token for password restoration and stores it in the database.
         * Then sends an email to the user
         * @param string $email Email address of the user
         * @throws Nette\Security\AuthenticationException
         */
        public function enableResetPassword($email)
        {
            $user = $this->database->table('AUTHORS')->where('EMAIL',$email)->fetch();
            if($user == NULL)
            {
                throw new Nette\Security\AuthenticationException('User not found');
            }
            else
            {
                $token = \Nette\Utils\Strings::random();
                $this->database->table('TOKENS')->insert(array
                        (
                            'TOKEN' => $token,
                            'EMAIL' => $email
                        ));

                $url = $_SERVER['SERVER_NAME'] .  '/blog/www/administration/passwordrestore/' . $token;
                
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= "From: korinek111@gmail.com" . "\r\n";
                
                mail($email,
                    'Blog - obnovení hesla',
                    "<html><body><p>Pro změnu hesla klikněte na následující odkaz: <a href='$url'>$url</a>  Důrazně doporučujeme jeho okamžitou změnu po přihlášení!</p></body></html>",
                    $headers);
            }
        }       
        
        /**
         * Resets user's password
         * @param string $token Token used for reset
         * @param string $newPassword New password for the user
         * @param string $email User's email for verification purposes
         */
        public function resetPassword($token,$newPassword,$email)
        {
            $tok = $this->database->table('TOKENS')->where('TOKEN',$token)->where('EMAIL',$email)->fetch();
            if($tok == NULL or $tok->USED != 'false' or ((time() - $tok->VALID->getTimestamp()) > 500))
            {
                throw new \Exception('Neplatný token!');
            }
            $hashed = Nette\Security\Passwords::hash($newPassword);
            $this->database->table('AUTHORS')->where('EMAIL',$email)->update(array('PASSWORD' => $hashed));
            $this->database->query('DELETE FROM TOKENS WHERE TOKEN = ?',$token);
        }
}
