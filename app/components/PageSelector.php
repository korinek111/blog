<?php

/**@author Lukáš Kořínek <korinek111@gmail.com>*/

namespace App\Components;

use Nette, App\Model;
use Nette\Application\UI\Control;

class PageSelector extends Control
{
    
    public function render($pageNumbers,$currentPage,$link)
    {        
        $this->template->setFile(__DIR__ . '/templates/pageSelector.latte');
        $this->template->currentPage = $currentPage;
        $this->template->pageNumbers = $pageNumbers;
        $this->template->link = $link;
        $this->template->render();
        
    }
}