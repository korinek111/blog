<?php

/**@author Lukáš Kořínek <korinek111@gmail.com>*/

namespace App\Components;

use Nette, App\Model;
use Nette\Application\UI\Control;

class ArticlePreview extends Control
{
    
    public function render($article,$commentCount,$showAuthor)
    {
        $this->template->setFile(__DIR__ . '/templates/articlePreview.latte');
        $this->template->article = $article;
        $this->template->commentCount = $commentCount;
        $this->template->showAuthor = $showAuthor;
        $this->template->render();
    }
}