<?php

/**@author Lukáš Kořínek <korinek111@gmail.com>*/

namespace App\Presenters;

use Nette;

/**
 * Homepage presenter which displays all articles
 */
class HomepagePresenter extends PagedPresenter
{
        /**
         * Renders articles
         * @param int $page Current page to be displayed
         */
	public function renderDefault($page = 1)
        {
		$this->template->articles = $this->articleManager->getArticles(NULL,NULL,($page - 1) * 5,5);
                $this->template->commentCounts = $this->articleManager->getCommentCounts($this->template->articles);
                parent::render($page,'',$this->articleManager->getArticleCount(NULL));
	}
        
}
