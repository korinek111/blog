<?php

/**  @author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette;
use Nette\Application\UI;
use Nette\Application\UI\Multiplier;

/**
 * Presenter for showing details of an article
 */
class ArticlePresenter extends BasePresenter
{                
    /**@persistent*/
    private $commentId;
    
    /**
     * Shows the article
     * @param uint $articleId Id of the article
     * @param mixed $commentId Id of the comment, which should be edited
     */
    public function renderShow($articleId)
    {        
        $this->template->articleId = $articleId;
        $this->template->commentId = $this->commentId;
        
        $this->template->article = $this->articleManager->getArticles($articleId,NULL,0,5);
        if($this->template->article == NULL)
        {
            $this->redirect('Homepage:default');
        }        
        $this->template->comments = $this->articleManager->getComments($articleId,$articleId);   
    }
    
    /**
     * Factory which creates a form for comment displaying
     * @param uint $articleId Id of the article
     * @return \Nette\Application\UI\Form
     */
    public function createComponentCommentForm()
    {
        $form = new \Nette\Application\UI\Form();
        $form->addHidden('commentId',$this->commentId);
        
        $content = $form->addTextArea('content', 'Text komentáře')
                        ->setRequired('Vyplňte prosím text komentáře');
        
        if ($this->commentId != NULL)
        {
            $comment = $this->articleManager->getCommentById($this->commentId);
            $content->setValue($comment->CONTENT);
            $form->addSubmit('submit', 'Uložit úpravy')->setAttribute('class', 'ajax');
        }
        else
        {
            $form->addSubmit('submit', 'Přidat komentář')->setAttribute('class', 'ajax');
        }
        $form->onSuccess[] = array($this,'commentFormSubmitted');
        $this->commentId = NULL;
        return $form;
    }
    
    /**
     * Callback for the comment form. Creates or edits comments.
     * @param \Nette\Application\UI\Form $form
     */
    public function commentFormSubmitted(UI\Form $form)
    {
        $articleId = $this->getParameter('articleId');
        try
        {
            if($form['commentId']->value == '')
            {
                $this->articleManager->addComment($articleId,$_SERVER['REMOTE_ADDR'],$this->getUser()->getId(),$form['content']->value);
            }
            else
            {
                $this->articleManager->editComment($form['commentId']->value,$form['content']->value);
            }
        }
        catch(\Exception $ex)
        {
            file_put_contents('debug.txt',$ex->getMessage());
            $this->flashMessage('Nastala chyba při vkládání komentáře!');
        }
        
        if($this->isAjax())
        {
            $form->setValues(array('content' => ''),TRUE);
            $this->redrawControl('commentsSnippet');
        }
    }
    
    /**
     * Handler for the 'delete comment' ajax request
     * @param uint $commentId Id of the comment
     */
    public function handleDelete($commentId)
    {
        $this->articleManager->deleteComment($commentId);
        $this->redrawControl('commentsSnippet');
    }
    
    /**
     * Handler for the 'edit comment' ajax request
     * @param uint $commentId Id of the comment
     */
    public function handleEdit($commentId)
    {
        $this->commentId = $commentId;
        $this->redrawControl('commentsSnippet');
    }
    
}