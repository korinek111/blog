<?php

/**  @author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette;

/**
 * Presenter for creation of RSS feed
 */
class RSSPresenter extends BasePresenter
{
    
    /**
     * Gets articles, which will be displayed in the feed
     */
    public function renderFeed()
    {
        $this->template->items = $this->articleManager->getArticles(NULL,NULL,0,10);
    }
}
