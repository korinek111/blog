<?php

/**  @author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette;
        
/**
 * Paged presenter for showing search results
 */
class SearchPresenter extends PagedPresenter
{
    
    /**
     * Renders search results
     * @param string $needle Input string used to search 
     * @param uint $page Number of a page to display
     */
    public function renderDefault($needle,$page = 1)
    {        
        $this->template->articles = $this->articleManager->findArticle($needle,(($page - 1) * 5),5);        
        $this->template->needle = $needle;
        $this->template->articleCount = count($this->template->articles);
        $this->template->commentCounts = $this->articleManager->getCommentCounts($this->template->articles);
        parent::render($page,$needle,$this->articleManager->getArticleCountForSearch($needle));
    }
    
}