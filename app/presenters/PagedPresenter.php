<?php

/**@author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette;

/**
 * Extends BasePresenter with support of paging
 */
abstract class PagedPresenter extends BasePresenter 
{  
    /**Current page selected in url*/
    private $currentPage;
    
    /**All numbers to be shown in component PageSelector*/
    private $pageNumbers;
   
    /**Saves numbers of pages into variables*/
    protected function solvePageNumbers($page,$count)
    {
          $pageCount = ceil($count / 5);

          $this->currentPage = $page;
          $this->pageNumbers = array();
                
          $helper = $this->currentPage;
          if($helper < 6) 
          {
              $helper = 6;
          }
                
          for($i = $helper - 5;$i < $helper + 4 and $i < $pageCount;$i++)
          {
             array_push($this->pageNumbers,$i);
          }               
                
          if(!in_array($pageCount, $this->pageNumbers))
          {
             array_push($this->pageNumbers,$pageCount);
          }
    }
    
    /**
     * Method, which should be called in every child. Appends important variables
     * into the template
     * @param uint $page Current page to display
     * @param string $link Link to the active page, which will be used in links
     */
    protected function render($page,$link,$count)
    {
        $this->solvePageNumbers($page,$count);
        $this->template->pageNumbers = $this->pageNumbers;
        $this->template->currentPage = $this->currentPage;
        $this->template->link = $link;
    }
    
    /**
     * Factory for creation of page selectors
     * @return \App\Components\PageSelector
     */
    public function createComponentPageSelector()
    {
        $selector = new \App\Components\PageSelector();
        return $selector;
    }   
    
}
