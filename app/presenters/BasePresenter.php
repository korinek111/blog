<?php

/**@author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette,App\Model;
use Nette\Application\UI;
use App\Components\ArticlePreview;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var Nette\Database\Context Database PDO object*/
    protected $database;
    
    /** ArticleManager class instance, for database operations in model*/
    protected $articleManager;
    
    /**@var Nette\Security\User Logged user*/
    protected $user;
    
    /**
     * Constructs the presenter and creates a PDO database. The database is then stored in $database variable.
     * @param Nette\Database\Context $database
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
        $this->articleManager = new Model\ArticleManager($this->database);
    }
    
    /**
     * Checks if a user is logged in and puts the information into the template
     */
    protected function beforeRender() {
        parent::beforeRender();
        
        if($this->getUser()->isLoggedIn())
        {
            $this->template->user = $this->getUser()->identity->getData()[0];
        }
        else
        {
            $this->template->user = NULL;
        }
        $this->template->action = $this->getName();
    }
    
    /**
     * Dummy renderer. Logouts the user and redirects to the homepage
     */
    public function renderLogout()
    {
        if($this->getUser()->isLoggedIn())
        {
            $this->getUser()->logout();
        }
        $this->flashMessage('Byl jste odhlášen');
        $this->redirect('Homepage:default');
    }
    
    /**
     * Factory creating the search form component
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentSearchForm()
    {
        $form = new UI\Form();
        $form->addText('input','Vyhledávání')
             ->setRequired('Napište, co hledáte')
             ->setAttribute('placeholder','Najít na blogu');        
        $form->addSubmit('search','');
        $form->onSuccess[] = array($this,'doSearch');
        return $form;
    }
    
    /**
     * Callback for the search form. Redirects to the search presenter
     * @param \Nette\Application\UI\form $form
     */
    public function doSearch(UI\form $form)
    {
        $this->redirect('Search:default',$form['input']->value);
    }
    
    /**
     * Factory for creating the ArticlePreview component
     * @return ArticlePreview
     */
    protected function createComponentArticlePreview()
    {
        $control = new ArticlePreview();
        return $control;
    }
    
    /**
     * Factory for creation of the login form
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentLoginForm()
    {
        $form = new UI\Form();
        $form->addGroup('Přihlášení');
        $form->addText('email','Email:')
             ->setType('email')
             ->setAttribute('type','email')
             ->setAttribute('placeholder','uživatel@doména.com')
             ->setAttribute('maxlength','80');
        
        $form->addPassword('password', 'Heslo:')
             ->setRequired('Vyplňte prosím heslo');
        $form->addSubmit('submit','Přihlásit se');
        
        $form->onSubmit[] = array($this,'loginFormSubmitted');
        return $form;
    }
    
    /**
     * Callback for the login form. Logs in the user.
     * @param \Nette\Application\UI\Form $form
     */
    public function loginFormSubmitted(UI\Form $form)
    {
            try
            {
                $this->getUser()->login($form['email']->value,$form['password']->value);
                $this->flashMessage('Byl jste přihlášen');
                $this->redirect('this');
            } 
            catch (Nette\Security\AuthenticationException $ex) 
            { 
                $this->flashMessage($ex->getMessage());
            }
    }
}
