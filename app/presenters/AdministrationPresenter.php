<?php

/* * @author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI;

/**
 * Presenter for all admin and user operations
 */
class AdministrationPresenter extends BasePresenter {

    /**
     * @var \App\Model\UserManager
     * @inject
     */
    public $userManager;

    /** @persistent */
    private $articleId;

    /**
     * Private method for checking permissions of the user.
     * Redirects in case of violation.
     */
    private function checkPermission() 
    {
        if (!$this->getUser()->isLoggedIn()) 
        {
            $this->redirect('Homepage:default');
        }
    }

    /**
     * Overrides the parent´s method
     * Calls the checkPermission method
     */
    protected function beforeRender() 
    {
        parent::beforeRender();
        if ($this->getView() != 'forgotPassword' and $this->getView() != 'register' and $this->getView() != 'passwordrestore') 
        {
            $this->checkPermission();
        }
    }

    /**
     * Private method used for image upload
     * @param FileUpload $img File upload object sent as a form value
     * @return string Path to the saved image
     */
    private function uploadImage($img) 
    {
        if ($img->isImage() and $img->isOk()) 
        {
            $extension = strtolower(mb_substr($img->getSanitizedName(), strrpos($img->getSanitizedName(), ".")));
            $path = '../www/images/articles/' . \Nette\Utils\Strings::random() . $extension;

            $img->move($path);
            $img = \Nette\Utils\Image::fromFile($path);
            
            if ($img->getHeight() > 150) 
            {
                $img->resize(NULL, 150);
            } 
            if ($img->getWidth() > 300) 
            {
                $img->resize(300,NULL);
            }
            $img->save($path);

            return $path;
        } 
        else
        {
            return NULL;
        }
    }

    /**
     * Renders the administration menu with article list
     */
    public function renderMenu() 
    {
        $this->template->articles = $this->articleManager->getArticles(NULL, $this->getUser()->getId(), 0, 100);
        $this->template->articleCount = $this->articleManager->getArticleCount(NULL);
    }

    /**
     * Renders the new article form
     */
    public function renderNewArticle() 
    { }

    /**
     * Renders the edit article form
     * @param uint $articleId Id of the article
     */
    public function renderEdit($articleId) 
    {
        $this->articleId = $articleId;
        // Byl zde požadavek, aby byly články editovány pomocí ajaxu. 
        // Nevím jak bych to sem mohl zakomponnovat.
    }

    /**
     * Renders the form for password recovery
     */
    public function renderForgotPassword() 
    {
        $this->template->hideLogin = TRUE;
    }
    
    public function renderPasswordRestore($token)
    {
        $this->template->token = $token;
    }

    /**
     * Renders the form for new user registration
     */
    public function renderRegistration() 
    {
        $this->template->hideLogin = TRUE;
    }

    /**
     * Creates a form, which will be used for new user registration
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentRegistrationForm() 
    {
        $form = new UI\Form();
        $form->addGroup('Registrovat nového uživatele');
        
        $form->addText('email', 'Email:')
             ->setRequired('Zdejte prosím Váš email')
             ->setType('email')
             ->setAttribute('type','email')
             ->setAttribute('maxlength', 80);
        
        $form->addText('nick', 'Přezdívka:')
             ->setRequired('Zadejte prosím Vaši přezdívku')
             ->setAttribute('maxlength', 60)
             ->setAttribute('minlength', 5);
        
        $form->addPassword('password', 'Heslo:')
             ->setRequired('Zadejte prosím Vaše heslo')
             ->setAttribute('minlength', 5)
             ->setAttribute('maxlength', '20');
        
        $form->addPassword('passwordVerify', 'Heslo znova:')
             ->setRequired('Zopakujte prosím své heslo')
             ->setAttribute('minlength', 5)
             ->setAttribute('maxlength', '20');
        
        $form->addSubmit('submit', 'Registrovat se!');
        $form->onSuccess[] = array($this, 'registrationFormSubmitted');
        return $form;
    }

    /**
     * Callback for the registration form
     * @param \Nette\Application\UI\Form $form
     */
    public function registrationFormSubmitted(UI\Form $form) 
    {
        if ($form['password']->value == $form['passwordVerify']->value) 
        {
            $message = $this->userManager->addUser($form['email']->value, $form['nick']->value, $form['password']->value);

            if ($message != 'OK') 
            {
                $this->flashMessage($message);
            } 
            else
            {
                $this->flashMessage('Uživatel zaregistrován!');
                $this->redirect('Homepage:default');
            }
        }
        else
        {
            $this->flashMessage('Hesla se musejí shodovat');
        }
    }

    /**
     * Factory, which creates the new article form.
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentNewArticleForm()
    {
        $form = new UI\Form();
        $form->addText('header', 'Nadpis článku:')
             ->setAttribute('maxlength', 60)
             ->setRequired('Prosím zadejte nadpis článku!');
        
        $form->addUpload('file', 'Nahrát obrázek k článku:')
             ->setRequired('Přidejte k článku obrázek!');
        
        $form->addTextArea('content', 'Obsah článku:')
             ->setRequired('Zadejte obsah článku!')
             ->setAttribute('rows', 15);
        
        $form->addSubmit('submit', 'Vytvořit článek');
        $form->onSubmit[] = array($this, 'newArticleFormSubmitted');
        return $form;
    }

    /**
     * Callback for the new article form
     * @param \Nette\Application\UI\Form $form
     */
    public function newArticleFormSubmitted(UI\Form $form)
    {
        $img = $form['file']->value;

        $path = $this->uploadImage($img);
        if ($path != NULL)
        {
            $this->articleManager->createArticle($form['header']->value, $path, $this->getUser()->getId(), $form['content']->value);
            $this->flashMessage('Článek vytvořen!');
            $this->redirect('menu');
        }
        else
        {
            $this->flashMessage('Neplatný obrázek!');
        }
    }

    /**
     * Factory which creates a new article form
     * @return \Nette\Application\UI\Form
     * @throws \Nette\Application\BadRequestException
     */
    protected function createComponentEditArticleForm()
    {
        $id = $this->gethttpRequest()->getQuery('articleId');

        $article;
        // Valid article id check
        try
        {
            $article = $this->articleManager->getArticles($id, NULL, 0, 10);
            if ($article == NULL)
            {
                throw new \Nette\Application\BadRequestException('Neexistující id!');
            }
        }
        catch (\Exception $ex)
        {
            return $this->createComponentNewArticleForm();
        }
        // Valid article id check

        $form = new UI\Form();
        $form->addText('header', 'Nadpis článku:')
             ->setAttribute('minlength', 10)
             ->setAttribute('maxlength', 60)
             ->setRequired('Prosím zadejte nadpis článku!')
             ->setValue($article->HEADER);
        
        $form->addUpload('file', 'Nahrát obrázek k článku')
             ->setValue($article->IMAGE)
             ->setRequired('Přidejte k článku obrázek!');
        
        $form->addTextArea('content', 'Obsah článku:')
             ->setRequired('Zadejte obsah článku!')
             ->setAttribute('rows', 15)
             ->setValue(str_replace('<p>', '', str_replace('</p>', '\n', $article->CONTENT)));
        
        $form->addHidden('articleId', $id);
        $form->addSubmit('submit', 'Uložit změny');
        $form->onSubmit[] = array($this, 'editArticleFormSubmitted');

        return $form;
    }

    /**
     * Callback for the edit article form
     * @param \Nette\Application\UI\Form $form
     */
    public function editArticleFormSubmitted(UI\Form $form)
    {
        $img = $form['file']->value;

        $path = $this->uploadImage($img);
        if ($path != NULL)
        {
            $this->articleManager->editArticle($form['articleId']->value, $form['header']->value, $path, $form['content']->value);
            $this->flashMessage('Článek upraven!');
            $this->redirect('menu');
        }
        else
        {
            $this->flashMessage('Neplatný obrázek, nelze nahrát!');
        }
    }

    /**
     * Factory, which creates the form for password recovery
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentForgotPasswordForm()
    {
        $form = new UI\Form();
        
        $form->addText('email', 'Váš email: ')
             ->setRequired('Prosím vyplňte email k odeslání!')
             ->setType('email')
             ->setAttribute('placeholder', 'adresa@doména.com')
             ->setAttribute('type', 'email');
        
        $form->addSubmit('submit', 'Obnovit heslo');
        $form->onSuccess[] = array($this, 'forgotPasswordFormSubmitted');
        return $form;
    }

    /**
     * Callback for the forgotPasswordForm
     * @param \Nette\Application\UI\Form $form
     */
    public function forgotPasswordFormSubmitted(UI\Form $form)
    {
        try
        {
            $this->userManager->enableResetPassword($form['email']->value);
            $this->flashMessage('Odeslán email s odkazem pro změnu');
            $this->redirect('Homepage:default');
        }
        catch (\Exception $ex)
        {
            $this->flashMessage('Bohužel tato adresa u nás není zaregistrovaná');
        }
    }
    
    /**
     * Factory which creates a new form used for password restoration
     */
    protected function createComponentRestorePasswordForm()
    {
        $token = $this->getParameter('id');
        $form = new UI\Form();
        $form->addGroup();
        
        $form->addText('email','Ještě jednou váš email:')
             ->setType('email')
             ->setAttribute('type','email')
             ->setAttribute('maxlength','80')
             ->setAttribute('placeholder','adresa@doména.com')
             ->setRequired('Musím ověřit váš email, zadejte jej prosím');
        
        $form->addPassword('password','Nové heslo:')
             ->setRequired('Zadejte prosím vaše nové heslo');
        
        $form->addPassword('passwordVerify','Nové heslo znovu:')
             ->setRequired('Musíte své heslo zadat dvakrát pro ověření!');
        
        $form->addHidden('token',$token);
        $form->addSubmit('submit','Obnovit heslo');
        $form->onSuccess[] = array($this,'restorePasswordFormSubmitted');
        return $form;
    }
    
    /**
     * Callback for the restorePasswordForm
     */
    public function restorePasswordFormSubmitted(UI\Form $form)
    {
        $values = $form->getValues();
        
        if($values->password == $values->passwordVerify)
        {
            try
            {
                $this->userManager->resetPassword($values->token, $values->password, $values->email);
            } 
            catch (\Exception $ex) 
            {
                $this->flashMessage('Nepodařilo se ověřit email nebo obnovit heslo');
            }
            $this->redirect('Homepage:default');
        }
        else
        {
            $this->flashMessage('Hesla se musí shodovat!');
        }
        
    }

    /**
     * Ajax method, which deletes articles specified by the $articleId argument
     * @param uint $articleId Id of the article
     */
    public function handleDelete($articleId)
    {
        $this->checkPermission();

        try
        {
            $this->articleManager->deleteArticle($articleId);
            $this->flashMessage('Článek byl úspěšně smazán');
        }
        catch (\Exception $ex)
        {
            $this->flashMessage('Nepodařilo se smazat článek!');
        }

        $this->template->articles = $this->articleManager->getArticles(NULL, $this->getUser()->getId(), 0, 100);
        $this->template->articleCount = $this->articleManager->getArticleCount(NULL);
        $this->redrawControl('articleTable');
    }

}
