<?php

/**  @author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette;
        
/**
 * Paged presenter showing some basic information about the author and then
 * rendering all his articles
 */
class AuthorPresenter extends PagedPresenter
{
    
    /**
     * Default renderer. Shows information about the author and his articles.
     * @param uint $id Id of the author
     * @param uint $page Current page to be displayed
     * @return void
     */
    public function renderProfile($id,$page = 1)
    {        
        $this->template->author = $this->articleManager->getAuthors($id);
        if($this->template->author == NULL)
        {
            $this->redirect('Homepage:default');
            return;
        }
        
        $this->template->articles = $this->articleManager->getArticles(NULL,$id,($page - 1) * 5,5);
        $this->template->articleCount = count($this->template->articles);
        $this->template->commentCounts = $this->articleManager->getCommentCounts($this->template->articles);
        parent::render($page,$id,$this->articleManager->getArticleCount($id));
    }
    
}