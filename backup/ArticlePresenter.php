<?php

/**  @author Lukáš Kořínek <korinek111@gmail.com> */

namespace App\Presenters;

use Nette;
use Nette\Application\UI;

/**
 * Presenter for showing details of an article
 */
class ArticlePresenter extends BasePresenter
{            
    
    /**@persistent*/
    private $articleId;
    
    /**@persistent*/
    private $commentId;
    
    /**
     * Shows the article
     * @param uint $id Id of the article
     */
    public function renderShow($id)
    {        
        $this->template->id = $id;
        $this->articleId = $id;
        $this->template->article = $this->articleManager->getArticles($id,NULL,0,5);
        if($this->template->article == NULL)
        {
            $this->redirect('Homepage:default');
        }        
        $this->template->comments = $this->articleManager->getComments($this->template->article->ARTICLE_ID,$this->template->article->ARTICLE_ID);   
        
        if($this->commentId != NULL)
        {
            $this->template->editCommentId = $this->commentId;
        }
        else
        {
            $this->template->editCommentId = NULL;
        }
    }
    
    /**
     * Factory which creates a form for comment displaying
     * @param uint $articleId Id of the article
     * @return \Nette\Application\UI\Form
     */
    public function createComponentCommentForm()
    {
        $form = new UI\Form();
        
        $content = $form->addTextArea('content','Text komentáře')
             ->setRequired('Vyplňte prosím text komentáře');
        $form->addHidden('articleId',$this->articleId);
        
        if($this->commentId != NULL)
        {
           $comment = $this->articleManager->getCommentById($this->commentId);
           $content->setValue($comment->CONTENT);
           $form->addHidden('commentId',$this->commentId);
           $form->addSubmit('submit','Uložit úpravy')->setAttribute('class','ajax');
           $this->commentId = NULL;
        }
        else
        {
           $form->addHidden('commentId','');
           $form->addSubmit('submit','Přidat komentář')->setAttribute('class','ajax');
        }
        
        $form->onSuccess[] = array($this,'commentFormSubmitted');
        return $form;
    }
    
    /**
     * Callback for the comment form. Creates comments.
     * @param \Nette\Application\UI\Form $form
     */
    public function commentFormSubmitted(UI\Form $form)
    {
        try
        {
            if($form['commentId']->value != '')
            {
                $this->articleManager->editComment($form['commentId']->value,$form['content']->value);
            }
            else
            {
                $this->articleManager->addComment($form['articleId']->value,$_SERVER['REMOTE_ADDR'],$this->getUser()->getId(),$form['content']->value);
            }
        }
        catch(\Exception $ex)
        {
            file_put_contents('debug.txt',$ex->getMessage());
            $this->flashMessage('Nastala chyba při vkládání komentáře!');
        }
        
        if($this->isAjax())
        {
            $form->setValues(array('content' => ''),TRUE);
            $this->redrawControl('commentsSnippet');
        }
    }
    
    /**
     * Handler for the 'delete comment' ajax request
     * @param uint $commentId Id of the comment
     */
    public function handleDelete($commentId)
    {
        $this->articleManager->deleteComment($commentId);
        $this->redrawControl('commentsSnippet');
    }
    
    /**
     * Handler for the 'edit comment' ajax request
     * @param uint $commentId Id of the comment
     */
    public function handleEdit($commentId)
    {
        $this->commentId = $commentId;
        $this->redrawControl('commentsSnippet');
    }
    
}